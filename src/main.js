// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import VueScrollTo from 'vue-scrollto';
import AOS from 'aos';
// import pdf from 'vue-pdf';
// import connect from 'connect-history-api-fallback';
require('./assets/custom.css');
require('../node_modules/aos/dist/aos.css');
require('../node_modules/animate.css/animate.min.css');

Vue.config.productionTip = false;
Vue.use(VueScrollTo);
// Vue.use(pdf);
// Vue.use(
//   connect({
//     disableDotRule: true
//   })
// );

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>',
  created() {
    AOS.init({
      once: true
    });
  }
});
